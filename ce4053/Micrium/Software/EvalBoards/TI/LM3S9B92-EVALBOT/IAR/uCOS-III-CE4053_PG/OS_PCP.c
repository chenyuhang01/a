/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : OS_PCP.c
* Version       : V1.00
* Programmer(s) : Chen Yu Hang
*
*********************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include "OS_PCP.h"

/************************************************************************************************************************
*                                                 DATA STRUCTURE
************************************************************************************************************************/

/************************************************************************************************************************
*                                                       STACK
************************************************************************************************************************/

/************************************************************************************************************************
*                                                   GLOBAL VARIABLE
************************************************************************************************************************/

#define Stack_MAX_NUM_ELE 10
#define pow2(n) (1 << (n))

OS_TCB *GlobalStack[Stack_MAX_NUM_ELE];
OS_TCB *GlobalTempStack[Stack_MAX_NUM_ELE];
CPU_INT08U globalNumberOfElements;
OS_MUTEX globalMutex;

CPU_TS32 StartTime,StartTime2;

/************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************/


//API for Global stack
//Initial global stack
void globalStackInit(void)
{
  CPU_INT08U count;
  OS_ERR err;  
  globalNumberOfElements = (CPU_INT08U)0u;
    
  for(count = 0; count < Stack_MAX_NUM_ELE; count++)
  {
    GlobalStack[count] = (OS_TCB*)0u;
    GlobalTempStack[count] = (OS_TCB*)0u;
  }
  OSMutexCreate ((OS_MUTEX *)&globalMutex, (CPU_CHAR *)2, (OS_ERR *)&err);
}

//Push TCB into the global stack
CPU_BOOLEAN globalStackPush(OS_TCB *ptr_tcb)
{
 //Check if the stack is full, if is full, return false
  if(globalStackisFull())
    return 0;
 
  CPU_INT08S counter = 0;
  CPU_INT08U temp_counter = 0;
  CPU_INT08U curNumberOfElements = globalNumberOfElements;
  
  if(curNumberOfElements != 0u)
  {
    while(ptr_tcb->Prio > GlobalStack[curNumberOfElements-1]->Prio)
    {
      GlobalTempStack[counter] = globalStackPop();
      curNumberOfElements = globalNumberOfElements;
      counter++;
      if(curNumberOfElements == 0)
         break;
    }
    
    
    GlobalStack[curNumberOfElements] = ptr_tcb;
    curNumberOfElements++;
    while(counter > 0)
    {
      GlobalStack[curNumberOfElements+temp_counter] = GlobalTempStack[counter-1];
      GlobalTempStack[counter-1] = (OS_TCB*)0u;
      counter--;
      temp_counter++;
    }  
    
  }
  else
  {
    GlobalStack[curNumberOfElements] = ptr_tcb;
    //printf("\n");
  }
  
  globalNumberOfElements += 1u + temp_counter;
  //Return true if the number is pushed into the stack successfully
  return 1;  
}

//Pop the TCB from the global stack
OS_TCB * globalStackPop(void)
{
  OS_TCB * returnTCB = (OS_TCB *)0u;
  
  //Check if the stack is full, if is full, return false
  if(globalStackisEmpty())
    return 0;
  
  //Access the top values
  returnTCB = GlobalStack[globalNumberOfElements-1];
  
  //Initialize the top stack;
  GlobalStack[globalNumberOfElements-1] = (CPU_INT08U) 0u;
  globalNumberOfElements--;
 
  //Return true if the number is pushed into the stack successfully
  return returnTCB;    
}

//Check if statck is full
CPU_BOOLEAN globalStackisFull()
{
  if(globalNumberOfElements == ((CPU_INT08U)Stack_MAX_NUM_ELE))
  {
    //printf("Stack is full\n");
    return 1;
  }
  else
     return 0;  
  
}

//check if stack is empty
CPU_BOOLEAN globalStackisEmpty()
{
  if(globalNumberOfElements == ((CPU_INT08U)0u))
  {
    //printf("Stack is empty\n");
    return 1;
  }
  else
     return 0;   
  
}

//Get the top data element of the stack, without removing it
OS_TCB * globalStack_peek()
{
  OS_TCB * returnTCB = (OS_TCB *)0u;
  
  //Check if the stack is full, if is full, return false
  if(globalStackisEmpty())
    return 0;
  
  //Access the top values
  returnTCB = GlobalStack[globalNumberOfElements-1];
 
  //Return true if the number is pushed into the stack successfully
  return returnTCB;   
  
  
}


/************************************************************************************************************************
*                                                       AVL TREE
************************************************************************************************************************/
/************************************************************************************************************************
*                                                 DATA STRUCTURE
************************************************************************************************************************/
/************************************************************************************************************************
*                                                 GLOBAL VARIABLE
************************************************************************************************************************/
//AVL Tree
//AVL Tree Node
OS_MEM       avltreeNodeMEM;
AVLNode      alloMemavlNode[40];   //1 row for 40 words of AVLNode

//AVL Tree
OS_MEM       avlTreeMEM;
AVLTree      alloMemavlTree[1];    //1 row for 1 word of AvlTree

//AVL Tree reference
AVLTree *avlTree = (AVLTree *)0;

/************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************/

//Initialize the AVL tree
void AVL_Init(void)
{
    //Allocate Memory for Binary Nodes
    
    OS_ERR err;
     
    printf("Initializing AVL tree\n");
    //Allocate Memory to AVL Tree nodes
    OSMemCreate((OS_MEM         *)&avltreeNodeMEM,
                (CPU_CHAR       *)"AVLNodePartition",
                (void           *)&alloMemavlNode[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(AVLNode),
                (OS_ERR         *)&err);
    
    
    //Allocate Memory to AVL tree
    OSMemCreate((OS_MEM         *)&avlTreeMEM,
                (CPU_CHAR       *)"AVLTreePartition",
                (void           *)&alloMemavlTree[0],
                (OS_MEM_QTY      ) 2,
                (OS_MEM_SIZE     ) sizeof(AVLTree),
                (OS_ERR         *)&err);
    

    //Initialize nodelists
    avlTree = (AVLTree*)OSMemGet(
                                      (OS_MEM  *)&avlTreeMEM, 
                                      (OS_ERR  *)&err);
    
    avlTree->rootNode = (AVLNode*)0;
}

//Insert into the AVL Tree
AVLNode *AVL_Insert(AVLNode* node, OS_MUTEX *p_mutex)
{ 
  /*1. Perform the normal BST insertion*/
  
  if(node == (AVLNode*)0)
     return AVL_CreateNewNode(p_mutex);
  
  if(p_mutex->resource_ceiling <= node->p_mutex->resource_ceiling)
     node->left = AVL_Insert(node->left,p_mutex);
  else if( p_mutex->resource_ceiling > node-> p_mutex->resource_ceiling)
     node->right = AVL_Insert(node->right,p_mutex);
  else  
     return node;
  
  /*2 Update height of this ancestor node*/
  node->height = 1 + AVL_max(AVL_GetHeight(node->left),
                             AVL_GetHeight(node->right));
  
  /*3 Get the balance factor of this ancestor node to check 
      whether this node became unbalanced
  */
  CPU_INT08S balance = AVL_GetBalance(node);
  
  //If this node becomes unbalanced, then 
  //there are 4 cases
  
  //Left Left Case
  if(balance > 1 && p_mutex->resource_ceiling < node->left->p_mutex->resource_ceiling)
     return AVL_rotateRight(node);
  
  //Right Right Case
  if(balance < -1 && p_mutex->resource_ceiling < node->right->p_mutex->resource_ceiling)
     return AVL_rotateLeft(node);
  
  //Left Right Case
  if(balance > 1 && p_mutex->resource_ceiling > node->left->p_mutex->resource_ceiling)
  {
    node->left = AVL_rotateLeft(node->left);
    return AVL_rotateRight(node);
  }
  
  //Right Left Case
  if(balance < -1 && p_mutex->resource_ceiling < node->right->p_mutex->resource_ceiling)
  {
    node->right = AVL_rotateRight(node->right);
    return AVL_rotateLeft(node);
  }  
  
  /*return unchanged the node pointer*/
  return node;
}

//Delete the Node
AVLNode * AVL_Delete(AVLNode* root,OS_MUTEX *p_mutex)
{
  //Step 1: Perform Standard BST delete
  
  if(root == (AVLNode*)0)
     return root;
  
  //if the key to be deleted is smaller than the 
  //root's key, then it lies in left subtree
  if(p_mutex->resource_ceiling < root->p_mutex->resource_ceiling)
     root->left = AVL_Delete(root->left,p_mutex);
  
  else if(p_mutex->resource_ceiling > root->p_mutex->resource_ceiling)
     root->right = AVL_Delete(root->right,p_mutex);
  
  //if key is same as root's key, then This is 
  //the node to be deleted
  else
  {
     //node with only one child or no child
     if((root->left == (AVLNode*)0)||(root->right == (AVLNode*)0))
     {
       AVLNode *temp = root->left?root->left:root->right;
      
       //No child case
       if(temp == (AVLNode*)0)
       {
         temp = root;
         root = (AVLNode*)0;
       }
       else     //One child case
       {
         *root = *temp;          //Copy the oontents of the non-empty child
       } 
       AVL_FreeNode(temp);       
     }
     else
     {
       //node with two children: Get the inorder
       //successor (smallest in the right subtree)
        AVLNode *temp = AVL_minValueNode(root->right);
        
        //Copy the inorder successor's data to this node
        root->p_mutex = temp->p_mutex;
        
        //Delete the inorder successor
        root->right = AVL_Delete(root->right,temp->p_mutex);
     }
  }
  
  //if the tree had only one node then return
  if(root == (AVLNode*)0)
     return root;
  
  //Step 2: update the height of the current node
  root->height = 1 + AVL_max(AVL_GetHeight(root->left),
                             AVL_GetHeight(root->right));
  
  //Step 3: Get the balance factor of this node
  CPU_INT08S balance = AVL_GetBalance(root);
  
  //If this node becomes unbalanced, then 
  //there are 4 cases
  
  //Left Left Case
  if(balance > 1 && AVL_GetBalance(root->left) >= 0)
     return AVL_rotateRight(root);
  
  //Left Right Case
  if(balance > 1 && AVL_GetBalance(root->left) < 0)
  {
    root->left = AVL_rotateLeft(root->left);
    return AVL_rotateRight(root);    
    
  }
  //Right Right Case
  if(balance < -1 && AVL_GetBalance(root->right) <= 0)
  {
    return AVL_rotateLeft(root);
  }
  
  //Right Left Case
  if(balance < -1 && AVL_GetBalance(root->right) > 0)
  {
    root->right = AVL_rotateRight(root->right);
    return AVL_rotateLeft(root);
  }
  
  return root;
}

/*Helper Function*/

//LeftRotate
AVLNode * AVL_rotateLeft(AVLNode *x)
{
  AVLNode *y = x->right;
  AVLNode *T2 = y->left;
  
  //Perform rotation
  y->left = x;
  x->right = T2;
  
  //Update heights 
  x->height = AVL_max(AVL_GetHeight(x->left),AVL_GetHeight(x->right))+1;
  y->height = AVL_max(AVL_GetHeight(y->left),AVL_GetHeight(y->right))+1;
 
  //Return new root
  return y;
}

//RightRotate
AVLNode * AVL_rotateRight(AVLNode *y)
{
  AVLNode *x = y->left;
  AVLNode *T2 = x->right;
  
  //Perform rotation
  x->right = y;
  y->left = T2;
  
  //Update heights
  y->height = AVL_max(AVL_GetHeight(y->left),AVL_GetHeight(y->right))+1;
  x->height = AVL_max(AVL_GetHeight(x->left),AVL_GetHeight(x->right))+1;
  
  //Return new root
  return x;
}

//Given a non-empty binart search tree, return the 
//node with minimum data value found in that tree
//Note that the entire tress does not need to be searched
AVLNode *AVL_minValueNode(AVLNode* node)
{
  AVLNode *current = node;
  
  //Loop down to find the leftmost leaf
  while(current->left != (AVLNode*)0)
     current = current->left;
  return current;
}

//Get Balance Factor of node N
CPU_INT08S AVL_GetBalance(AVLNode *N)
{
  if(N == (AVLNode*)0)
     return 0;
  return AVL_GetHeight(N->left) -  AVL_GetHeight(N->right);
  
}
//Get the maximum of two integers
CPU_INT08S AVL_max(CPU_INT08S a,CPU_INT08S b)
{
  return (a > b) ? a : b;
}
               
//Get the height of the tree
CPU_INT08S AVL_GetHeight(AVLNode *N)
{
  if(N == (AVLNode *)0)
     return 0;
  return N->height;
}

//Print the Tree
void AVL_PrintTree()
{
  print(avlTree->rootNode);
}
//Recursive Print
void print(AVLNode* node)
{
  if(node == (AVLNode*)0)
    return;
  print(node->left);
  printf("%s: %d\n",node->p_mutex->NamePtr,node->p_mutex->resource_ceiling);
  print(node->right);
}

//Create a new node
AVLNode *AVL_CreateNewNode(OS_MUTEX *p_mutex)
{
    OS_ERR err;
    AVLNode *newNode = (AVLNode*)0;
    //Initialize newNode
    newNode = (AVLNode*)OSMemGet(
                                      (OS_MEM  *)&avltreeNodeMEM, 
                                      (OS_ERR  *)&err);  
    newNode->p_mutex = p_mutex;
    newNode->left = (AVLNode*)0;
    newNode->right = (AVLNode*)0;
    newNode->height = 1;
    
    
    return newNode;
}

//Free the memory
void AVL_FreeNode(AVLNode *node)
{
  OS_ERR err;
  OSMemPut((OS_MEM *) &avltreeNodeMEM,
           (void   *) node,
           (OS_ERR *) &err);  
}

/*API function of AVL Tree*/

void AVL_InsertData(OS_MUTEX *p_mutex)
{
  avlTree->rootNode = AVL_Insert(avlTree->rootNode,p_mutex);
}
void AVL_DeleteData(OS_MUTEX *p_mutex)
{
  avlTree->rootNode = AVL_Delete(avlTree->rootNode,p_mutex);
}

CPU_INT08U AVL_min_resourceCeiling(void)
{
  if(avlTree->rootNode == (AVLNode*)0)
    return 25u;
  else
    return (AVL_minValueNode(avlTree->rootNode))->p_mutex->resource_ceiling;
}

OS_TCB *AVL_min_Task(void)
{
  if(avlTree->rootNode == (AVLNode*)0)
    return (OS_TCB*)0u;
  else
    return (AVL_minValueNode(avlTree->rootNode))->p_mutex->OwnerTCBPtr;
}

/************************************************************************************************************************
*                                                       USER API
************************************************************************************************************************/

//Create Mutex
void OS_PCP_MUTEX_CREATE(OS_MUTEX    *p_mutex,
                         CPU_CHAR    *p_name,
                         OS_ERR      *p_err,
                         CPU_INT08U   resource_ceiling)
{
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to be called from an ISR                   */
        *p_err = OS_ERR_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    p_mutex->Type              =  OS_OBJ_TYPE_MUTEX;        /* Mark the data structure as a mutex                     */
    p_mutex->NamePtr           =  p_name;
    p_mutex->OwnerTCBPtr       = (OS_TCB       *)0;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)0;         /* Mutex is available                                     */
    p_mutex->TS                = (CPU_TS        )0;
    p_mutex->OwnerOriginalPrio =  OS_CFG_PRIO_MAX;
    p_mutex->resource_ceiling = resource_ceiling;
    p_mutex->tasks_waiting = 0u;
    
#if OS_CFG_DBG_EN > 0u
    OS_MutexDbgListAdd(p_mutex);
#endif
    OSMutexQty++;

    CPU_CRITICAL_EXIT();
    *p_err = OS_ERR_NONE;  
}

//Pend on the resource mutex
void OS_PCP_MUTEX_PEND(OS_MUTEX   *p_mutex,
                   OS_TICK     timeout,
                   OS_OPT      opt,
                   CPU_TS     *p_ts,
                   OS_ERR     *p_err)
{
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();
    
    CPU_INT08U system_ceiling;
    CPU_INT08U origPrio;
/*******************************************************************
                          Parameter checks
*******************************************************************/    
    
#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_PEND_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate arguments                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
    switch (opt) {
        case OS_OPT_PEND_BLOCKING:
        case OS_OPT_PEND_NON_BLOCKING:
             break;

        default:
             *p_err = OS_ERR_OPT_INVALID;
             return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif
    
/*******************************************************************
                        Parameter checks  Ends
*******************************************************************/

//Run PCP protocol
    
    CPU_CRITICAL_ENTER();
    //Get system ceiling
    system_ceiling = AVL_min_resourceCeiling();
    
    /* Point to the TCB of the Mutex owner                    */
    p_tcb = AVL_min_Task();
    //If the priority of the task is smaller
    while(OSTCBCurPtr->Prio >= system_ceiling && OSTCBCurPtr->resoucreHolding==0u)
    {

      //The job that causes this blocking raised to the same priority as
      //the job is blocking
      if (p_tcb->Prio > OSTCBCurPtr->Prio) {
          origPrio = p_tcb->Prio;
          p_tcb->Prio = OSTCBCurPtr->Prio;                
          BinaryHeap_Heapify();           
          printf("%s priority changed from %d to %d \n",p_tcb->NamePtr,origPrio,p_tcb->Prio);
      }     
      
      printf("%s request for Resoucre %d\n",OSTCBCurPtr->NamePtr,p_mutex->NamePtr);     
      OS_SYSTEM_CEILING_PEND();
      system_ceiling = AVL_min_resourceCeiling();
    }  
    
    
    
    //If there is no owner of the mutex and 
    //current task priority higher than system ceiling or currently holding the mutex
    //the resource is granted
    if ((p_mutex->OwnerTCBPtr == (OS_TCB*)0u) 
        && (OSTCBCurPtr->Prio < system_ceiling || OSTCBCurPtr->resoucreHolding!=0u)) {
        
        printf("%s request for Resoucre %d, Granted\n",OSTCBCurPtr->NamePtr,p_mutex->NamePtr);
        
        //Increase number of resources used by the current task
        OSTCBCurPtr->resoucreHolding++;                 
        
        //Let the mutex owner be this current task
        p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;
        
        //Store its orignal priority
        p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
        
        //Insert into AVL_Tree and system ceiling updates automatically
        AVL_InsertData(p_mutex); 
        
        p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
        //printf("System ceiling = %d\n",AVL_min_resourceCeiling());
        CPU_CRITICAL_EXIT();
        *p_err                     =  OS_ERR_NONE;
        return;
    }

    //If the task request for the same resource again
    //does nothing
    
    if (OSTCBCurPtr == p_mutex->OwnerTCBPtr) {              /* See if current task is already the owner of the mutex  */
        p_mutex->OwnerNestingCtr++;
        if (p_ts != (CPU_TS *)0) {
           *p_ts  = p_mutex->TS;
        }
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_OWNER;                        /* Indicate that current task already owns the mutex      */
        return;
    }
    
  

    if ((opt & OS_OPT_PEND_NON_BLOCKING) != (OS_OPT)0) {    /* Caller wants to block if not available?                */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_PEND_WOULD_BLOCK;                   /* No                                                     */
        return;
    } else {
        if (OSSchedLockNestingCtr > (OS_NESTING_CTR)0) {    /* Can't pend when the scheduler is locked                */
            CPU_CRITICAL_EXIT();
            *p_err = OS_ERR_SCHED_LOCKED;
            return;
        }
    }
    
    CPU_CRITICAL_ENTER();
    switch (OSTCBCurPtr->PendStatus) {
        case OS_STATUS_PEND_OK:                             /* We got the mutex                                       */
             break;

        case OS_STATUS_PEND_ABORT:                          /* Indicate that we aborted                               */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_PEND_ABORT;
             break;

        case OS_STATUS_PEND_TIMEOUT:                        /* Indicate that we didn't get mutex within timeout       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = (CPU_TS  )0;
             }
             *p_err = OS_ERR_TIMEOUT;
             break;

        case OS_STATUS_PEND_DEL:                            /* Indicate that object pended on has been deleted        */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_OBJ_DEL;
             break;

        default:
             *p_err = OS_ERR_STATUS_INVALID;
             break;
    }
    CPU_CRITICAL_EXIT();
    
}

//Post on the resource mutex
void OS_PCP_MUTEX_POST(OS_MUTEX *p_mutex,
                       OS_ERR   *p_err,
                       OS_OPT    opt)
{
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    OS_TCB *TaskInGlobalStack;
    CPU_INT08U system_ceiling;
    
    CPU_INT08U first = 0;
    
    CPU_INT08U origPrio = 0;
    
    
    CPU_SR_ALLOC();

/*******************************************************************
                        Parameter checks 
*******************************************************************/
 

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_POST_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    if (OSTCBCurPtr != p_mutex->OwnerTCBPtr) {              /* Make sure the mutex owner is releasing the mutex       */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_NOT_OWNER;
        return;
    }

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();
    p_mutex->OwnerNestingCtr--;                             /* Decrement owner's nesting counter                      */
    if (p_mutex->OwnerNestingCtr > (OS_NESTING_CTR)0) {     /* Are we done with all nestings?                         */
        OS_CRITICAL_EXIT();                                 /* No                                                     */
        *p_err = OS_ERR_MUTEX_NESTING;
        return;
    }
/*******************************************************************
                        Parameter checks  Ends
*******************************************************************/
    
    //Number of resources holding by current task decrements
    OSTCBCurPtr->resoucreHolding--;  
    
   
    
    //No Owner now
    p_mutex->OwnerTCBPtr = 0u;
   
    //Get the system ceiling
    TaskInGlobalStack = globalStack_peek();
    
    first = 1;
    AVL_DeleteData(p_mutex);

    //Acquire system ceiling
    system_ceiling = AVL_min_resourceCeiling();
    
    //Those tasks in the blocked list has higher priority than the 
    //system ceiling are added back to the ready list.
    //Choose among the highest priority from these tasks
    //and assign the mutex to it.
    //if not mutex owner remains empty

    while(!globalStackisEmpty() && TaskInGlobalStack->Prio < system_ceiling && TaskInGlobalStack != (OS_TCB*)0)
    {
       BinaryHeap_Insert(globalStackPop());
       //OS_Post(
       //         (OS_PEND_OBJ *)&globalMutex,
       //         (OS_TCB      *)TaskInGlobalStack,
       //         (void        *)0,
       //         (OS_MSG_SIZE  )0,
       //          1000000u);       
       
       OS_PCPTaskReady(TaskInGlobalStack);                                  /* Make task ready to run                            */
             TaskInGlobalStack->TaskState  = OS_TASK_STATE_RDY;
             TaskInGlobalStack->PendStatus = OS_STATUS_PEND_OK;              /* Clear pend status                                 */
             TaskInGlobalStack->PendOn     = OS_TASK_PEND_ON_NOTHING;        /* Indicate no longer pending                        */       
          printf("%s inserts into the readyList\n",TaskInGlobalStack->NamePtr);      
          if (OSTCBCurPtr->Prio != p_mutex->OwnerOriginalPrio) { 
              origPrio = OSTCBCurPtr->Prio;
              OSTCBCurPtr->Prio = p_mutex->OwnerOriginalPrio; 
              BinaryHeap_Heapify();
              printf("%s priority changed to %d from %d\n",OSTCBCurPtr->NamePtr,OSTCBCurPtr->Prio,origPrio);
          }             
          BinaryHeap_Heapify();
          first = 0;
              
      TaskInGlobalStack = globalStack_peek();
      system_ceiling = AVL_min_resourceCeiling();
    }
    
    //printf("System ceiling = %d\n",AVL_min_resourceCeiling());
    OS_CRITICAL_EXIT_NO_SCHED();
    
    if ((opt & OS_OPT_POST_NO_SCHED) == (OS_OPT)0) {
        if(OSTCBCurPtr->resoucreHolding == 0)
           OSSched();                                          /* Run the scheduler                                      */
    }

    *p_err = OS_ERR_NONE;  
}

//Pend if the system ceiling is lower than current executing task                   
void OS_SYSTEM_CEILING_PEND()
{
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();
    OS_ERR *p_err;

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();                  /* Lock the scheduler/re-enable interrupts                */
    
    
    //OS_Pend(&pend_data,                                     /* Block task pending on Mutex                            */
     //       (OS_PEND_OBJ *)&globalMutex,
    //         OS_TASK_PEND_ON_MUTEX,
    //         1000000u);
    
    OS_PCPTaskBlock(OSTCBCurPtr,1000000u);
    OSTCBCurPtr->PendStatus = OS_STATUS_PEND_OK;
    OS_CRITICAL_EXIT_NO_SCHED();
    
    //Inserting into block list  
    //Remove from ready List
    CPU_CRITICAL_ENTER();
    printf("%s is blocked and add into the blocked list\n",OSTCBCurPtr->NamePtr);
    globalStackPush(OSTCBCurPtr);
    BinaryHeap_deleteNode(OSTCBCurPtr);
    CPU_CRITICAL_EXIT(); 
    OSSched();                                              /* Find the next highest priority task ready to run       */
    
    CPU_CRITICAL_ENTER();
    switch (OSTCBCurPtr->PendStatus) {
        case OS_STATUS_PEND_OK:                             /* We got the mutex                                       */
             *p_err = OS_ERR_NONE;
             break;

        case OS_STATUS_PEND_ABORT:                          /* Indicate that we aborted                               */
             *p_err = OS_ERR_PEND_ABORT;
             break;

        case OS_STATUS_PEND_TIMEOUT:                        /* Indicate that we didn't get mutex within timeout       */
             *p_err = OS_ERR_TIMEOUT;
             break;

        case OS_STATUS_PEND_DEL:                            /* Indicate that object pended on has been deleted        */
             *p_err = OS_ERR_OBJ_DEL;
             break;

        default:
             *p_err = OS_ERR_STATUS_INVALID;
             break;
    }
    CPU_CRITICAL_EXIT();  
}

//Actual Task that block the task
void OS_PCPTaskBlock(OS_TCB   *p_tcb,
                    OS_TICK   timeout)
{
    OS_ERR  err;
    if (timeout > (OS_TICK)0) {                             /* Add task to tick list if timeout non zero               */
        if (err == OS_ERR_NONE) {
            p_tcb->TaskState = OS_TASK_STATE_PEND_TIMEOUT;
        } else {
            p_tcb->TaskState = OS_TASK_STATE_PEND;
        }
    } else {
        p_tcb->TaskState = OS_TASK_STATE_PEND;
    }  
}

//Acutal Task that unblock the task
void OS_PCPTaskReady(OS_TCB *p_tcb)
{
    //OS_TickListRemove(p_tcb);                               /* Remove from tick list                                  */  
}