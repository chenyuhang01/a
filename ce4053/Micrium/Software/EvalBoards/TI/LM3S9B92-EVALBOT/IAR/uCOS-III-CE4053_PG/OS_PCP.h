/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : OS_PCP.h
* Version       : V1.00
* Programmer(s) : Chen Yu Hang
*
*********************************************************************************************************
*/


/************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************/

#include "os.h"
#include "OSRecTask.h"

/************************************************************************************************************************
*                                                       STACK
************************************************************************************************************************/
/************************************************************************************************************************
*                                                 DATA STRUCTURE
************************************************************************************************************************/
//Just an array which declared in OS_PCP.c


/************************************************************************************************************************
*                                                 FUNCTION PROTOTPYE
************************************************************************************************************************/

//Initial global stack
void globalStackInit(void);

//Push TCB into the global stack
CPU_BOOLEAN globalStackPush(OS_TCB *ptr_tcb);

//Pop the TCB from the global stack
OS_TCB * globalStackPop(void);

//Check if statck is full
CPU_BOOLEAN globalStackisFull(void);

//check if stack is empty
CPU_BOOLEAN globalStackisEmpty(void);


//Get the top data element of the stack, without removing it
OS_TCB * globalStack_peek();


/************************************************************************************************************************
*                                                       AVL TREE
************************************************************************************************************************/
/************************************************************************************************************************
*                                                 DATA STRUCTURE
************************************************************************************************************************/

typedef struct _avlNode{
    OS_MUTEX * p_mutex;

    struct _avlNode *left;
    struct _avlNode *right;
    CPU_INT08S height;
}AVLNode;


typedef struct _avltree{
    AVLNode *rootNode;
}AVLTree;


/************************************************************************************************************************
*                                                 FUNCTION PROTOTPYE
************************************************************************************************************************/

/*Main AVL Tree Function*/

//Initialize the AVL tree
void AVL_Init(void);

//Insert into the AVL Tree
AVLNode * AVL_Insert(AVLNode* node,OS_MUTEX * p_mutex);

//Delete the Node
AVLNode * AVL_Delete(AVLNode* root,OS_MUTEX * p_mutex);

/*Helper Function*/

//LeftRotate
AVLNode * AVL_rotateLeft(AVLNode *x);

//RightRotate
AVLNode * AVL_rotateRight(AVLNode *y);

//Get the maximum of two integers
CPU_INT08S AVL_max(CPU_INT08S a,CPU_INT08S b);

//Get the height of the tree
CPU_INT08S AVL_GetHeight(AVLNode *N);

//Get Balance Factor of node N
CPU_INT08S AVL_GetBalance(AVLNode *N);

//Given a non-empty binart search tree, return the 
//node with minimum data value found in that tree
//Note that the entire tress does not need to be searched
AVLNode *AVL_minValueNode(AVLNode* node);

//Print the Tree
void AVL_PrintTree(void);

//Recursive Print
void print(AVLNode* node);


//Create a new node
AVLNode *AVL_CreateNewNode(OS_MUTEX * p_mutex);
void AVL_FreeNode(AVLNode *node);

/*API function of AVL Tree*/
void AVL_InsertData(OS_MUTEX * p_mutex);
void AVL_DeleteData(OS_MUTEX * p_mutex);

CPU_INT08U AVL_min_resourceCeiling(void);
OS_TCB *AVL_min_Task(void);
/************************************************************************************************************************
*                                                       USER API
************************************************************************************************************************/

//Create Mutex
void OS_PCP_MUTEX_CREATE(OS_MUTEX    *p_mutex,
                         CPU_CHAR    *p_name,
                         OS_ERR      *p_err,
                         CPU_INT08U   resource_ceiling); 

//Pend on the resource mutex
void OS_PCP_MUTEX_PEND(OS_MUTEX   *p_mutex,
                   OS_TICK     timeout,
                   OS_OPT      opt,
                   CPU_TS     *p_ts,
                   OS_ERR     *p_err);

//Post on the resource mutex
void OS_PCP_MUTEX_POST(OS_MUTEX *p_mutex,
                       OS_ERR   *p_err,
                       OS_OPT    opt);

//Pend if the system ceiling is lower than current executing task                   
void OS_SYSTEM_CEILING_PEND();                   
 
//Actual Task that block the task
void OS_PCPTaskBlock(OS_TCB   *p_tcb,
                    OS_TICK   timeout);

//Acutal Task that unblock the task
void OS_PCPTaskReady(OS_TCB *p_tcb);
